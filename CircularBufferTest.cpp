#define BOOST_TEST_MODULE MyTest
#include <boost/test/unit_test.hpp>
#include <algorithm>
#include "CircularBuffer.hpp"


void printArray(CircularBuffer<int>& cb) {
    for (auto &val : cb) {
        std::cout << "\t" << val << std::endl;
    }
}

const int maxSize = 5;

BOOST_AUTO_TEST_CASE(pointers_test) {
    CircularBuffer<int> circBuffer(maxSize);
    circBuffer.push_back(1);
    circBuffer.push_back(2);
    circBuffer.push_back(3);
    circBuffer.push_back(4);
    circBuffer.push_back(5);
    circBuffer.pop_front(); // remove 1, size 4
    circBuffer.pop_front(); // remove 2, size 3
    BOOST_CHECK(circBuffer.size() == 3);
    circBuffer.push_back(6); // size 4
    circBuffer.push_back(7); // size 5 [max]
    try { // should throw error since at max
        circBuffer.push_back(8);
        BOOST_CHECK(false);
    } catch (const std::overflow_error & e) {
        BOOST_CHECK(true);
    }

    int front = circBuffer.pop_front();
    BOOST_CHECK(front == 3);
    front = circBuffer.pop_front();
    BOOST_CHECK(front == 4);
    front = circBuffer.pop_front();
    BOOST_CHECK(front == 5);
    front = circBuffer.pop_front();
    BOOST_CHECK(front == 6);
    front = circBuffer.pop_front();
    BOOST_CHECK(front == 7);
    circBuffer.print_pointers();
    try { // should throw error since empty
        circBuffer.pop_front();
        BOOST_CHECK(false);
    } catch (const std::runtime_error & e) {
        BOOST_CHECK(true);
    }
}

// for use in following tests

BOOST_AUTO_TEST_CASE(size_test) {
    CircularBuffer<int> circBuffer(maxSize);
    circBuffer.push_back(1);
    circBuffer.push_back(2);
    circBuffer.push_back(3);
    BOOST_CHECK(circBuffer.size() == 3);
}

BOOST_AUTO_TEST_CASE(too_many_elements) {
    CircularBuffer<int> circBuffer(maxSize);
    circBuffer.push_back(1);
    circBuffer.push_back(2);
    circBuffer.push_back(3);
    circBuffer.push_back(4);
    circBuffer.push_back(5);
    try {
        circBuffer.push_back(6);
        BOOST_CHECK(false);
    } catch (const std::overflow_error & e) {
        BOOST_CHECK(true);
    }
    BOOST_CHECK(circBuffer.size() == 5);
}

BOOST_AUTO_TEST_CASE(too_few_elements) {
    CircularBuffer<int> circBuffer(maxSize);
    try {
        circBuffer.pop_front();
        BOOST_CHECK(false);
    } catch (const std::runtime_error & e) {
        BOOST_CHECK(true);
    }
}

BOOST_AUTO_TEST_CASE(pop) {
    CircularBuffer<int> circBuffer(maxSize);
    circBuffer.push_back(1);
    circBuffer.push_back(2);
    circBuffer.push_back(3);
    circBuffer.push_back(4);
    std::cout << "Before:" << std::endl;
    printArray(circBuffer);
    int front = circBuffer.pop_front();
    std::cout << "After:" << std::endl;
    printArray(circBuffer);
    BOOST_CHECK(front == 1);
}

BOOST_AUTO_TEST_CASE(iterator) {
    CircularBuffer<int> circBuffer(maxSize);
    circBuffer.push_back(1);
    circBuffer.push_back(2);
    circBuffer.push_back(3);
    circBuffer.push_back(4);
    circBuffer.push_back(5); // [1,2,3,4,5]
    circBuffer.pop_front();  // [2,3,4,5]
    circBuffer.pop_front();  // [3,4,5]
    std::cout << "Iterator test array:" << std::endl;
    printArray(circBuffer);
    auto it = circBuffer.begin();
    int x = *it;
    BOOST_CHECK(x == circBuffer.pop_front()); // [4, 5]
    int i = 4;
    std::cout << "Entering range based for loop" << std::endl;
    for (auto val : circBuffer) {
        std::cout << "\t" << val << std::endl;
        BOOST_CHECK(val == i);
        i++;
    }
}

