#ifndef _CIRC_BUFFER
#define _CIRC_BUFFER

#include <memory>
#include <string>

template <typename T>
class CircularBuffer {
public:
    CircularBuffer(int size);
    ~CircularBuffer();

    struct Iterator {
    public:
        Iterator(std::shared_ptr<T []> inPtr, int inIdx, int inMax) { 
            idx=inIdx; ptr=inPtr; maxSize=inMax; 
        };
        Iterator(const Iterator& i) { 
            idx = i.idx;
            maxSize = i.maxSize;
            ptr = i.ptr;
        }
        ~Iterator() {};
        T& operator* () { return ptr[idx]; };
        Iterator& operator++ ();
        Iterator& operator++ (T);
        friend bool operator== (Iterator& lhs, Iterator& rhs) { return rhs.idx == lhs.idx; };
        friend bool operator!= (const Iterator& lhs, const Iterator& rhs) { return rhs.idx != lhs.idx; };
    private:
        int idx;
        int maxSize;
        std::shared_ptr<T []> ptr;
    };

    unsigned int size();
    void push_back(T inVal);
    T pop_front();
    Iterator begin() { return Iterator(data, front, maxSize); };
    Iterator end() { return Iterator(data, back, maxSize); };

#ifdef DEBUG
    void print_pointers() {std::cout << "front: " 
                             << front << ", back: " 
                             << back << std::endl;};
#endif


private:
    std::shared_ptr<T []> data; 
    const unsigned int maxSize;
    unsigned int front;
    unsigned int back;
    unsigned int currentSize;
};



template <typename T>
CircularBuffer<T>::CircularBuffer(int size) : maxSize(size) {
    data = std::shared_ptr<T []> (new T [maxSize]);
    front = 0;
    back = front;
    currentSize = 0;
}

template <typename T>
CircularBuffer<T>::~CircularBuffer() {
}

template <typename T>
unsigned int CircularBuffer<T>::size() {
    return currentSize;
}

template <typename T>
void CircularBuffer<T>::push_back(T inVal) {
    if (currentSize >= maxSize) {
        throw std::overflow_error("CircularBuffer overflow");
    }
    data[back] = inVal;
    currentSize++;
    back++;
    if (back >= maxSize) {
        back = 0;
    }
}

template <typename T>
T CircularBuffer<T>::pop_front() {
    if (currentSize == 0) {
        throw std::runtime_error("CircularBuffer empty");
    }
    int retVal = data[front];
    currentSize--;
    front++;
    if (front >= maxSize) {
        front = 0;
    }
    return retVal;
}

template <typename T>
typename CircularBuffer<T>::Iterator& CircularBuffer<T>::Iterator::operator++ () {
    idx++;
    if (idx >= maxSize) {
        idx -= maxSize;
    }
    return *this;
}

template <typename T>
typename CircularBuffer<T>::Iterator& CircularBuffer<T>::Iterator::operator++ (T inVal) {
    idx++;
    if (idx >= maxSize) {
        idx -= maxSize;
    }
    return *this;
}






#endif
